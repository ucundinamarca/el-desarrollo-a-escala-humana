/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 17:34:36
 * @modify date 2020-12-12 23:31:25
 * @desc Libreria para la inclusion de preguntas en videos de youtube
 * @version 0.1
 */
var ivo={};
var load = false;
var respuestas = ['','','','','',''];
ivo.video = function(customSettings){
    let t=this;
    // Default settings
    var settings = {
        videoId : true,
        questions:[],
      /* and so on... */
    };
    //extendemos el parametro//
    customSettings || ( customSettings = {} );
    Object.assign( settings, customSettings );
    t.load=function(){
        //Script de youtube
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
    if(!load){
        load=true;
        t.load();
    }else{
        clearInterval(t.ivoplay.seconds);
        t.ivoplay.loadVideoById({'videoId':settings.videoId});
    }
    t.set_video=function(){
        t.ivoplay={};
        t.ivoplay = new YT.Player('player', {
            height:  settings.height,
            width:   settings.width,
            videoId: settings.videoId,
            playerVars: {
                'loop' : 1,
                'playlist': settings.videoId,
                'html5': '1',
                'cc_load_policy': '0',
                'disablekb': '1',
                'iv_load_policy': '3',
                'modestbranding': '1',
                'showinfo': '0',
                'rel': '0',
                'autoplay': '0',
            },
            events: {
                'onReady':       t.onPlayerReady,
                'onStateChange': t.onPlayerStateChange,
                'onError'      : t.onError
            }
        });
    }
    t.onError=function(){}
    //load video//
    t.onPlayerReady=function(event) {
        //play video una vez que carga
        event.target.playVideo();
    }
    //analizador tiempo
    t.onPlaychange=function(){
        clearInterval(t.ivoplay.seconds);
        t.ivoplay.seconds = setInterval(function () {
            console.log(settings.questions);
            for (question of settings.questions){
                try {
                    let seg = Math.round(t.ivoplay.getCurrentTime());
                    console.log(seg+" "+question.second);
                    if(seg == question.second ){
                        t.ivoplay.pauseVideo();
                        clearInterval(t.ivoplay.seconds);
                        t.throw_question(question);
                    }
                    if (seg >= 409) {
                        t.ivoplay.pauseVideo();
                        clearInterval(t.ivoplay.seconds);
                        t.resumen();
                    }
                } catch (error) {
                }
            }
        }, 1000);
    }
    //lanzador de preguntas//
    t.throw_question=function(q){
        let question=`<div>${q.question}</div>`;
        let options=q.answers;
        
        let answers = `
        <div class="input-group mb-3">
            <input  id="${options}" type="text" class="form-control box-ivo" placeholder="Ingrese su respuesta">
            <div class="input-group-append">
                <button data-id="${options}" class="btn btn-success ivo-options" type="submit">Envíar</button>
            </div>
        </div>
        `;
    

        let html=
        `<div data-msg-correct="${q["msg-correct"]}" data-msg-incorrect="${q["msg-incorrect"]}" id="ivo-play"> 
            <div> ${question} ${answers} </div>
        </div>`;

        settings.onQuestionShow();
        document.getElementById(settings.questionId).innerHTML=html;
    }
    t.resumen = function () {
        console.log("respues");
        let respuesta = '';
        let index = 0;
        for (question of settings.questions) { 
            respuesta += `<li> ${question.question} <span style="display:block;">${respuestas[index]}</span></li>`;
            index++;
        }

        let test=  `<ul>${respuesta}</ul>`;
        let html=
        `<div  id="ivo-play-final"> 
            <div> ${test} </div>
        </div>`;

        settings.onQuestionShow();
        document.getElementById(settings.questionId).innerHTML=html;
     }
    //eventos preguntas//
    t.events_question=function(){
        document.attachEvent = function( evt, q, fn ) {
		    document.addEventListener( evt, ( e ) => {
                if (e.target.matches( q ) ) {
                 fn.apply( e.target, [e] );
             }
            });
        };
        document.attachEvent('click','.ivo-options', function() {
            try {
                console.log($(this).attr('data-id'));
                let value = $("#" + $(this).attr("data-id")).val();
                let index = parseInt($(this).attr("data-id").split("input")[1])-1;
                respuestas[index]=value;
                settings.onNextQuestion();
                t.ivoplay.playVideo();
            } catch (error) {}
        });
        document.attachEvent('keyup','.box-ivo', function(e) {
            try {
                //detectamos el entero para saber enviar
                if (e.key === 'Enter' || e.keyCode === 13) {
                    // Do something
                    document.getElementsByClassName('ivo-options')[0].click();
                }
            } catch (error) {}
        });
    }
    t.events_question();
    //cambios de estado de reproduccion
    t.onPlayerStateChange=function(event) {
        switch (event.data) {
            case YT.PlayerState.PLAYING:
                t.onPlaychange();
                break;
            case YT.PlayerState.PAUSED:
                break;
            case YT.PlayerState.ENDED:
                clearInterval(t.ivoplay.seconds);
               

                break;
        }
    }
    window.onYouTubeIframeAPIReady = t.set_video;
    return settings;
};