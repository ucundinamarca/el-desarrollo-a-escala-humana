/**
 * @author Edilson Laverde Molina
 * @email edilsonlaverde_182@hotmail.com
 * @create date 2020-12-12 18:45:48
 * @modify date 2020-12-18 09:48:07
 * @desc Videos REA3
 */
var videos = [{
    "id": "wQFjMSqQGV0",
    "titulo": "1. Bolsa de valores",
    "descripcion": "¿Qué haces para suplir tu necesidad de… Entendimiento?",
    "url": "tdjDSkbIlWA",
    "preguntas": [
        {
            "second": "226",
            "question": "¿Qué haces para suplir tu necesidad de… Entendimiento?",
            "answers": "input1"
        }, {
            "second": "233",
            "question": "¿Qué haces para suplir tu necesidad de… Participación?",
            "answers": "input2"
        }, {
            "second": "240",
            "question": "¿Qué haces para suplir tu necesidad de… Ocio?",
            "answers": "input3"
        }, {
            "second": "247",
            "question": "¿Qué haces para suplir tu necesidad de… Creación?",
            "answers": "input4"
        }, {
            "second": "255",
            "question": "¿Qué haces para suplir tu necesidad de… Identidad?",
            "answers": "input5"
        }, {
            "second": "264",
            "question": "¿Qué haces para suplir tu necesidad de… Libertad?",
            "answers": "input6"
        }
    ]
}
];    

